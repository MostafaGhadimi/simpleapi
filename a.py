from flask import Flask, request, jsonify
from base64 import b64encode
import requests as r
import json

app = Flask(__name__)

@app.route('/GetSaleTokenNew', methods=['POST', 'GET'])
def func():
    if request.method == 'POST':
        URL = 'https://mpl.pec.ir/api/EShop/GetSaleTokenNew'
        # parse body data
        data = request.json

        # Mobile No, Amount, PaymentType, OrderId
        amount = data['Amount']
        mobile_no = data['MobileNo']
        order_id = data['OrderId']
        payment_type = data['PaymentType']

        # Authorization, App Version and Content-Type
        authorization = request.headers['Authorization']
        app_version = '2.0.0'
        content_type = 'application/json'

        # Setting Header Parameters
        headers = {
            'Authorization': authorization,
            'Content-Type': content_type,
            'appVersion': app_version
        }

        data = {
            'Amount': amount,
            'MobileNo': mobile_no,
            'OrderId': order_id,
            'PaymentType': payment_type
        }

        res = r.post(url=URL, headers=headers, json=data)

        # Parsing data
        res_bytes = res.content
        res_str = res_bytes.decode('utf-8').replace("'", '"')
        res_json = json.loads(res_str)

        return res_json

    return 'soltan GET kardi :))'
    
if __name__ == '__main__':
	app.run(host='0.0.0.0')

